module.exports.noticias = function(application, req, res) {
    var connection = application.config.db_connection();
    var noticiasModels = new application.app.models.NoticiasDAO(connection);

    noticiasModels.getNoticias(function(error, result) {
        res.render("noticias/noticias", {noticias: result});
    });
};

module.exports.noticia = function(application, req, res) {
    var connection = application.config.db_connection();
    var noticiasModels = new application.app.models.NoticiasDAO(connection);

    var dados = req.query;

    noticiasModels.getNoticia(dados.id_noticia, function(error, result) {
        console.log(result);
        res.render("noticias/noticia", {noticia: result[0]});
    });
};